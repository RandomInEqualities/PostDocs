.. _Authentication:

Authentication
==============

Authentication for the rest API is done by logging in with your mobile credentials and thereafter
maintaining an HTTP session. It uses HTTPS and a custom `digest authentication`_ as its security.

.. _`digest authentication`: https://en.wikipedia.org/wiki/Digest_access_authentication

Create Session
----------------

We create a persistent session, that should be active as long as you want to use the API. To
create the session send a PUT request to

::

    https://rest.e-boks.dk/mobile/1/xml.svc/en-gb/session

The request body should consist of the following logon XML

.. code:: xml

    <Logon xmlns="urn:eboks:mobile:1.0.0">
        <User identity="{cpr}" identityType="{identitytype}" nationality="{nationality}"
            pincode="{password}"/>
    </Logon>

Where the identifies in brackets are

cpr
    Set this to your CPR number. We have not tested it with CVR numbers.

identitytype
    Set to P for private account. We have not tested with company accounts.

nationality
    Set to DK or SE. We have only tested with Danish accounts.

password
    Set to your e-Boks mobile password. The password can be created on the e-Boks website.

In addition to the content type (application/xml) and content length headers. An
authentication header is required, see :ref:`authheadercreate`.

If successful the response will return some XML of the form

.. code:: xml

    <Session>
        <User userId="1234567" name="John Doe"/>
    </Session>

To make further queries you need to store the user id.

Close session
---------------

To end a session send a DELETE request to

::

    https://rest.e-boks.dk/mobile/1/xml.svc/en-gb/session

The authentication header is a little different from the in session authentication header. See
:ref:`authheaderclose`.

.. _authheadercreate:

Authentication header (Creating session)
----------------------------------------
The authentication header authenticates your identity. The authentication is done by both you and
the e-Boks servers computing and comparing a double hashed hex string containing hopefully the
same secret information. The authentication is successful if they match.

The authentication header should be

- **Name**: X-EBOKS-AUTHENTICATE

- **Value**: logon deviceid="{deviceid}", datetime="{datetime}", challenge="{challenge}"

The parameters in the value string should be

deviceid
    Any device id with format 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx' consisting of numbers (0-9)
    and letters (a-f).

datetime
    This value can be anything containing numbers or letters. The value is used for generating
    the challenge. It is meant to be the current date and time in isoformat.

challenge
    The challenge is a SHA-256 double hashed hex string. The hashed string should be
    '{activationcode}:{deviceid}:{identitytype}:{cpr}:{nationality}:{password}:{datetime}'.
    The values in the hash, except activationcode, are explained above. Activation code is
    the activation code for mobile applications, you can find it on e-boks.dk next to your
    mobile password.

With a successful authentication the response will also contain an authentication header as above
where

- **Name**: X-EBOKS-AUTHENTICATE

- **Value**: sessionid="{sessionid}", nonce="{nonce}"

Both session id and nonce should be extracted and stored. They are used for the in session
authentication.

Authentication header (In session)
----------------------------------
The session is maintained solely by sending an authentication header with each API request.
Every request that you send needs to have this authentication header.

The authentication header should consist of the following:

- **Name**: X-EBOKS-AUTHENTICATE

- **Value**: deviceid={deviceid},nonce={nonce},sessionid={sessionid},response={response}

The parameters in the value string should be

deviceid
    Can be any string. It does not appear to be checked. Meant to be the device id that you used
    to logon with.

nonce
    This value should be the nonce in the authentication header of the last response.

sessionid
    Should be the session id that you received at logon.

response
    This value is not checked, so you can set it to anything. Is meant to be a double hashed string
    of '{deviceid}:{nonce}:{sessionid}:{challenge}'. The challenge should be a SHA-256 double hashed
    hex string of '{activationcode}:{deviceid}:{nonce}:{sessionid}'. Where the values in the hash are
    explained above.

With each successful request the reponse will have an authentication header as above where

- **Name**: X-EBOKS-AUTHENTICATE

- **Value**: nonce="{nonce}"

The nonce that you currently have should be updated to this new value.

You can only have one outstanding API request at a time. So you send a request, wait for the
response, then extract the nonce in the response, and only now can you send the next API request.

.. _authheaderclose:

Authentication header (Closing session)
---------------------------------------
To close the session the authentication header is like the in session header, except that a
logoff keyword is prepended to the header value. So it should be

- **Name**: X-EBOKS-AUTHENTICATE

- **Value**: logoff deviceid={deviceid},nonce={nonce},sessionid={sessionid},response={response}

Where all the parameters in the value string is the same as the in session authentication
header.

.. _`Eboks root`:

E-boks root
-----------

To keep URL's short and readable we will use {eboks_root} to denote

::

    https://rest.e-boks.dk/mobile/1/xml.svc/en-gb/{userid}/0/mail

It acts as your personal e-Boks mail URL. All other queries in the upcoming sections are
relative to this. You should set **userid** to your user id that you got when creating the
e-Boks session.

For example if your userid is 1234567 the url {eboks_root}/folders is

::

    https://rest.e-boks.dk/mobile/1/xml.svc/en-gb/1234567/0/mail/folders

This makes it much easier to type API queries that you can do on your e-Boks.
