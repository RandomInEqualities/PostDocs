.. _Customers:

Customers
=========

An e-Boks customer is someone that can send mail on e-Boks. This can be either a private company or
a government body. The difference between an e-Boks sender and customer is that you have registered
with the senders, so that they can actually send you mail. Customers cannot send you mail until
you subscribe to them.

There are a few different ways to get a list of e-Boks customers. When you request a list of
customers the response will contain some XML of the form

.. code:: xml

    <Search>
        <Customers>
            <CustomerInfo custGrpId="1033" name="Danske Bank"></CustomerInfo>
            <CustomerInfo custGrpId="12098" name="Dansk Dekommissionering"></CustomerInfo>
            <CustomerInfo custGrpId="1221" name="Transport ApS"></CustomerInfo>
        </Customers>
    </Search>

Where there are a Customers tag with one or more CustomerInfo tags. Each CustomerInfo tag
describes one customer. The **custGrpId** is the customers id. The Search tag will have a
different name depending on the search method. It might also have some metadata about the
search.


E-Boks customer root
--------------------

We denote {eboks_customer_root} as

::

    https://rest.e-boks.dk/mobile/1/xml.svc/en-gb/{userid}/0/groups

Where **userid** should be the user id, that you got when completing the :ref:`Authentication`
section. This makes it much easier to type upcoming URL's. For example {eboks_customer_root}/segment
denotes the URL

::

    https://rest.e-boks.dk/mobile/1/xml.svc/en-gb/{userid}/0/groups/segment


Customers by category
---------------------

E-Boks customers are divided into categories. In this way you can for example get a list of all
customers that deals in real-estate. The categories are either for government bodies or private
companies.

To get a list of categories, send a GET request to

::

    {eboks_customer_root}/segment/{segment}

Where **segment** should be 'public' for a list of categories for government bodies and 'private'
for a list of private company categories. Both of these queries return some XML of the form

.. code:: xml

    <Segment name="public">
        <Categories>
            <CategoryInfo categoryId="5" customers="98" name="Municipalities">
            </CategoryInfo>
            <CategoryInfo categoryId="3648" customers="5" name="Regions">
            </CategoryInfo>
            <CategoryInfo categoryId="11853" customers="120" name="State">
            </CategoryInfo>
        </Categories>
    </Segment>

Where each CategoryInfo holds the information for one category. The **customers** attribute
describe the amount of customers in the category. The **categoryId** is used to get the actual
list of customers in that category.

To get a list of customers in a category, send a GET request to

::

    {eboks_customer_root}/category/{categoryid}

where you use the **categoryId** from the above category XML.

Example
^^^^^^^

Lets try to get a list of all banks that we can register with on e-Boks. We first get a list
of all private company categories, from a GET request to

::

    {eboks_customer_root}/segment/private

This will return something like this in its response body

.. code:: xml

    <Segment name="Private companies">
        <Categories>
            <CategoryInfo categoryId="15623" customers="23" name="Bolig og Ejendom">
            </CategoryInfo>
            <CategoryInfo categoryId="966" customers="85" name="Banks">
            </CategoryInfo>
            <CategoryInfo categoryId="8992" customers="5" name="Petrol and oil">
            </CategoryInfo>
            <CategoryInfo categoryId="973" customers="77" name="Electricity">
            </CategoryInfo>
        </Categories>
    </Segment>

There is a bank category (category id 966). We see that there are 85 banks. To get a list
of these customers we send a GET request to

::

    {eboks_customer_root}/category/966


Customers by search
-------------------

We can get a list of customers by searching for its starting letters. It will not search though
the complete customer name. Only the initial letters. To do this send a GET request to

::

    {eboks_customer_root}/customers?search={letters}

where **letters** should be a string that will be used to find all costumers whose names start
with that string.

For example, to find costumers whose names start with 'da', we send a GET request to

::

    {eboks_customer_root}/customers?search=da

This will return a response with an XML body of customers.


Customers by subscription
-------------------------

To get a list of customers that you have subscribed to, send a GET request to

::

    {eboks_customer_root}/subscriptions?list={segment}

Where **segment** should be public for a list of government bodies that you have subscribed
to and private for a list of private companies that you have subscribed to.


Customer information
--------------------

To get some information about a private segment customer, send a GET request to

::

    {eboks_customer_root}/customer/{customerid}/information

Where **costumerid** is the id of the costumer that you want information about. The request 
will fail for public segment customers (government bodies). It will return XML of the form

.. code:: xml

    <Information>
        <Sender></Sender>
        <Text></Text>
        <Link></Link>
        <Address></Address>
    </Information>

Each tag describes something about the customer. Note that these are very poorly filled out for 
most customers.


Subscriber groups
-----------------

Customers divide the different types of mail that they can send you into subscriber groups. For
example a bank might have two groups of mail, one for bank statements and another for everything
else. If you want to only receive bank statements over e-Boks you can subscribe to that group
only.

To get a list of subscriber groups (and a little metadata) of a customer, send a GET request to

::

    {eboks_customer_root}/customer/{customerid}

where you set **customerid** to the customers id. This will return some XML of the form

.. code:: xml

    <Customer custGrpId="1033" name="Danske Bank" subscription="0">
        <Description></Description>
        <Logo></Logo>
        <SubscrpGroups>
            <SubscrpGroupInfo name="BS-oversigt" subscription="0" subscrpId="5098">
            </SubscrpGroupInfo>
            <SubscrpGroupInfo name="Fonds og depot" subscription="0" subscrpId="4298">
            </SubscrpGroupInfo>
            <SubscrpGroupInfo name="Kontoudskrifter og lister" subscription="1" subscrpId="530">
            </SubscrpGroupInfo>
            <SubscrpGroupInfo name="Other" subscription="1" subscrpId="5311">
            </SubscrpGroupInfo>
        </SubscrpGroups>
    </Customer>

The SubscrpGroupInfo tags denotes the types of mail that the customer can send you. In this case
there are 4 groups of mail that you can receive from the customer. You should take note of the
**subscrpId**, as this is used for subscribing to the group.

Both the Customer tag and the SubscrpGroupInfo tags have a **subscribtion** attribute, which
tells if you are already subscribed to the customer and subscriber group. If you are subscribed
to the customer, then you are automatically subscribed to all its subscriber groups.

Subscriber group description
----------------------------

A text description for each subscriber group can be found by a GET request to

::

    {eboks_customer_root}/subscrpgroup/{subscribergroupid}

Where **subscribergroupid** should be the id of the subscriber group that you want a text
description for. This will return some XML of the form

.. code:: xml

    <SubscrpGroupInfo name="BS-oversigt" subscription="1" subscrpId="5098" type="P" >
        <CustomerInfo custGrpId="1033" name="Danske Bank"></CustomerInfo>
        <Description>Subscriber group description</Description>
    </SubscrpGroupInfo>

Subscribing
-----------

To subscribe or unsubscribe to a customer or subscriber group send a PUT request to

::

    {eboks_customer_root}/{subscriberid}?subscription={subscription}

Where **subscriberid** can be either a customer id or a subscriber group id. If it is a
customer id you subscribe to all mail from a customer. If it is a subscriber group id you
only subscribe to that group. The **subscription** parameter should be 'true' for
subscribing and 'false' for unsubscribing.
