.. _Folders:

Folders
=======

After establishing an e-Boks session we can begin to make API queries. In this section we explain
how to work with e-Boks folders. We use :ref:`{eboks_root} <Eboks root>` to shorten all API queries.

To get a list of folders in your e-Boks make a GET request to

::

    {eboks_root}/folders

This will return a response with an XML body of the form

.. code:: xml

    <Folders>
        <FolderInfo id="15567994" name="Inbox" type="B" unread="5"></FolderInfo>
        <FolderInfo id="31477288" name="Drafts" type="K" unread="0"></FolderInfo>
        <FolderInfo id="23846186" name="Custom Folder" type="O" unread="0">
            <FolderInfo id="23846187" name="Custom SubFolder" type="U" unread="0"></FolderInfo>
        </FolderInfo>
        <FolderInfo id="15567995" name="Deleted Items" type="P" unread="1"></FolderInfo>
        <FolderInfo id="31477287" name="Sent Items" type="S" unread="0"></FolderInfo>
    </Folders>

Each FolderInfo tag describes one folder. Folders can be nested inside each other. One
FolderInfo tag can contain several FolderInfo tags.

In each FolderInfo we have the folder **id** which uniquely identifies that folder. The id is 
used for operating on the folder.

The folders *Inbox*, *Drafts*, *Deleted Items* and *Sent Items* are default folders that
does what their name implies. These folders cannot be renamed, moved, deleted, etc.

Folders that you have created by yourself are of **type** O and U. Folders of type O are
top-level folders. Folders of type U are subfolders.

There is an **unread** attribute in each FolderInfo tag that are the number of unread messages
in that folder.

Create folder
-------------

To create a folder make a PUT request to

::

    {eboks_root}/folder/{parentid}?folderName={name}

where you should set

parentid
    To the folder that you want the new folder inside. Set it to 0 if you want the folder to be
    a top-level folder, with no parent folder.

name
    To the name of your new folder.

For example to create a folder named 'Archive' as a top level folder use

::

    {eboks_root}/folder/0?folderName=Archive

or if you want it inside a folder named 'MyFolder' with id 12345678

::

    {eboks_root}/folder/12345678?folderName=Archive

Delete folder
-------------

To delete a folder make a DELETE request to

::

    {eboks_root}/folder/{folderid}

Where you set **folderid** to the folder that you want deleted. It is only possible to delete
folders that has no messages inside them. The request will fail if the folder is non-empty.

Rename and move folder
----------------------

Renaming and moving folders are done by PUT requests to

::

    {eboks_root}/folder/{folderid}?newFolderName={newname}&toFolderID={newfolderid}

Where **folderid** should be the id of the folder that you want to rename and move. The parameters
in the request should be

newFolderName
    The new name of the folder.

toFolderID
    The new parent folder's id or 0 if you want to move it to the top-level, with no parent.

You need to specify both **newFolderName** and **toFolderID**. If you for example just want to
rename a folder, then you can set **toFolderID** to the current parent folder.

For example to rename a folder with id 5050832 to 'My Bank' and move it to a folder with id
12345678, we can use

::

    {eboks_root}/folder/5050832?newFolderName=My+Bank&toFolderID=12345678
