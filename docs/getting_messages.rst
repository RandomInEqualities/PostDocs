.. _`Getting Messages`:

Getting Messages
================

In this section we show the various ways to retrieve messages in your e-Boks acount. This 
section is on how to find messages. If you want to change a message or retrieve its text, see 
:ref:`Messages`.

Before using methods in this section you need to have completed the :ref:`Authentication` section.
Note that we use :ref:`{eboks_root} <Eboks root>` to shorten all e-Boks API queries.

There are several ways to find messages, latest messages, unread messages, from a folder,
from a specific sender and by searching. All these queries return XML of the form

.. code:: xml

    <SearchFolder>
    <Messages>
        <MessageInfo attachmentsCount="0" folderId="15567994" format="HTML" id="1234"
                name="Title1" receivedDateTime="2016-03-08T06:02:28" size="2752" unread="false">
            <Sender id="953">FerieKonto</Sender>
        </MessageInfo>
        <MessageInfo attachmentsCount="0" folderId="55545585" format="pdf" id="123456"
                name="Title2" receivedDateTime="2016-03-04T00:00:01" size="162572" unread="false">
            <Sender id="955">Naturstyrelsen</Sender>
            <Note>This message has a note.</Note>
        </MessageInfo>
    </Messages>
    </SearchFolder>

Where there generally are a big list of MessageInfo tags inside a Messages tag. Each MessageInfo
tag contains metadata for one message. The Note tag is only present if the message actually has 
a note. The attributes in each MessageInfo tag describes the message, its fairly obvious what 
they describe from their name. The **folderId** attribute is the folder that the message is 
located in.

All lists are sorted by receive time, with the most recent message first in the list.

Sometimes the SearchFolder tag will have some extra metadata on the search. The tag name can
also change between search methods. This is specific to each search method and will be explained 
there.

.. _`Skip and take parameters`:

Skip and take parameters
------------------------

Many requests for messages use skip and take URL parameters. The skip parameter is the amount
of message to jump over and take is the max amount of messages to return.

For example when settings skip to 5, e-Boks will always ignore the first 5 found messages.
Use this to get a big message list in chunks. First skip can be 0 and take 50, it will return
the first 50 messages, then set skip to 50 and it will return the next 50 messages and so on.
See :ref:`Unread messages` for an example.

Sometimes there are some metadata, like total amount of messages, etc, that you might want. If you
are only interested in this metadata and don't care about getting any message metadata, it is
possible to set both skip and take to 0.

Latest messages
---------------

To get a list of the latest messages, make a GET request to

::

    {eboks_root}/folder/search/latest

From our tests it returns 16 messages.

.. _`Unread messages`:

Unread messages
---------------

A list of unread messages can be retrieved by a GET request to

::

    {eboks_root}/folder/search/unread?skip={x}&take={y}

Where the skip and take parameters are used to navigate the list, see
:ref:`Skip and take parameters`.

For example to skip the first 25 unread messages found and then return at most 5 messages use

::

    {eboks_root}/folder/search/unread?skip=25&take=5

Messages between two dates
--------------------------

To get the messages between two dates make a GET request to

::

    {eboks_root}/folder/search/latest?from={from}&to={to}

Where **from** and **to** should be the date and time, in ISO format, of the start and end date.
This list can be quite big as it returns all messages between the two dates in the response.

For example to get all messages received christmas day 2015 you can use

::

    {eboks_root}/folder/search/latest?from=2015-12-24+00:00:00&to=2015-12-25+00:00:00

Messages from companies
-----------------------

To get a list of messages that a specific company or government body has sent you make a GET
request to

::

    {eboks_root}/folder/search/senders/{senderid}?skip={x}&take={y}

To get a list of senders and their ids see the :ref:`Senders` section. If you want a list of
messages that you have uploaded/sent to yourself, set **senderid** to 0.

The skip and take parameters are used to navigate the list, see :ref:`Skip and take parameters`.

The SearchFolder tag in the response XML also has a **messages** attribute that tells the total
amount of messages that you have from the sender.

As an example to get the 10 latest messages that you have uploaded send a GET request to

::

    {eboks_root}/folder/search/senders/0?skip=0&take=10

Messages inside folders
-----------------------

To get a list of messages that you have in a folder make a GET request to

::

    {eboks_root}/folder/{folderid}?skip={x}&take={y}

To get a list of folders and their ids, see the :ref:`Folders` section. The skip and take
parameters are used to navigate the list, see :ref:`Skip and take parameters`.

In this search method the SearchFolder tag is named Folder in the response XML. The tag 
have a **messages** attribute that tells the total amount of messages in the folder.

Search for message title
------------------------

To get a list of messages with a specific string in its title/name, make a GET request to

::

    {eboks_root}/folder/search?search={string}&skip={x}&take={y}

Where **string** is what you search for. The skip and take parameters are used to navigate the
list, see :ref:`Skip and take parameters`.
