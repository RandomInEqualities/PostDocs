Introduction
============

This is unofficial documentation for the rest API used by the official e-Boks_ mobile applications.
The rest API allows modification of e-Boks accounts without using any premade applications or
websites.

The mobile apps use a restful API to communicate with e-Boks account servers. The API have a very 
broad scope and you can almost do anything that is possible on e-boks.dk.

In :ref:`Authentication` we document how to establish your identity with e-Boks servers. In the
rest of the documents we describe how to use the rest API.

Use at your own risk (eboks.dk_).

Related Projects
----------------

Postboks_ is an Objective-C application to sync e-Boks mail on Mac OSX.

Net-Eboks_ is a Perl API built around the rest API. It can make e-Boks appear as a POP3 server.

.. _e-Boks: http://www.e-boks.dk
.. _eboks.dk: http://www.e-boks.dk/signup_terms.aspx?type=enduser
.. _Postboks: https://github.com/olegam/Postboks
.. _Net-Eboks: https://github.com/dk/Net-Eboks


.. toctree::
    :hidden:
    :maxdepth: 2

    self
    authentication
    folders
    senders
    getting_messages
    messages
    customers
