.. _Messages:

Messages
========

In this section we show how to operate on messages. If you are interested in finding messages and
their id's see the :ref:`Getting Messages` section. Before using this you need to have completed
the :ref:`Authentication` section. Note that we use :ref:`{eboks_root} <Eboks root>` to denote
the root URL of your e-boks account.

All HTTP requests in this section operates on

::

    {eboks_root}/folder/{folderid}/message/{messageid}

Where you should always set **folderid** to the folder that the message is currently located in
and **messageid** to the message id of the message that you want to perform an action on. We
describe all of the operations that you can perform on this URL and what added parameters can do.

.. _`Message metadata`:

Message metadata
----------------

To get all metadata for a message make a GET request to

::

    {eboks_root}/folder/{folderid}/message/{messageid}

This will return an XML response of the from

.. code:: xml

    <Message format="pdf" id="123345" name="Message Title" receivedDateTime="2015-01-23T05:01:41"
            size="30901" unread="false">
        <Sender id="955">Naturstyrelsen</Sender>
        <Note>This is a message note.</Note>
        <Attachments>
            <AttachmentInfo format="pdf" id="123345-4969" name="Attachment Title1" size="24233">
            </AttachmentInfo>
            <AttachmentInfo format="pdf" id="123345-4970" name="Attachment Title2" size="24233">
            </AttachmentInfo>
        </Attachments>
    </Message>

The Attachments and Note tags are only present if the message respectively have attachments and
a note. Each AttachmentInfo tag describes the metadata for one attachment.

The attributes in the Message tag does what their names would imply. There is a bug with the
**unread** attribute, it is a far as we know always false, no matter if the message is unread or
not.

For example, to get all metadata on a message with the id 20151337 in the folder myfolder with 
id 12345678, we send a GET request to

::

    {eboks_root}/folder/12345678/message/20151337

Message content
---------------

The message text is described by the **format** attribute in the metadata XML. If this is for example
pdf the text is a pdf file. The most common files are usually pdf, html or docx, but it can be
anything. The size in bytes of the message is given by the **size** attribute in the metadata.

To get the message text make a GET request to

::

    {eboks_root}/folder/{folderid}/message/{messageid}/content

This will return the message text in its response body.

Attachment content
------------------
To get the content of an attachment that a message has, send a GET request to

::

  {eboks_root}/folder/{folderid}/message/{attachmentid}/content

Note that we are using the attachment id, that you can find in the metadata XML, inside the
AttachmentInfo tags. In this tag you can also find the **format** (pdf, html, docs, etc.) and the
**size** in bytes of the attachment.

The attachment text is returned in the response body.

Rename message
--------------
To rename a message in your e-Boks, make a PUT request to

::

  {eboks_root}/folder/{folderid}/message/{messageid}?newName={newname}

where you should set **newname** to the new message title that you want.

For example, to rename a message named bankstuff with the id 20151337 in a folder with id
12345678 to bubber, we send a PUT request to

::

    {eboks_root}/folder/12345678/message/20151337?newName=bubber

Move message
------------

To move a message to another folder make a PUT request to

::

    {eboks_root}/folder/{folderid}/message/{messageid}?toFolderId={destinationid}

Set **destinationid** to the folder that you want to move the message to. After this operation,
new operations on the message need to be performed from this URL

::

    {eboks_root}/folder/{destinationid}/message/{messageid}

because the folder changed.

Delete message
--------------

To delete a message in your e-Boks, make a DELETE request to

::

    {eboks_root}/folder/{folderid}/message/{messageid}

If the message is in the deleted items folder, the message will be permanently deleted. If it is
in any other folder it will be moved to the deleted items folder, where it is permanently deleted
after 30 days.

For example, to delete a message named bankstuff with the id 20151111 in the
folder myfolder with id 12345678, we send a DELETE request to

::

    {eboks_root}/folder/12345678/message/20151111

the message is not immediately deleted, but moved to the deleted items folder.

Add message note
----------------

You can give each message in e-Boks a custom note. To add a note make a PUT request to

::

    {eboks_root}/folder/{folderid}/message/{messageid}?note={note}

Set **note** to the desired note. It will overwrite any note that the message currently has.

If you want to remove the current note, just set **note** to nothing i.e. make a PUT request to

::

    {eboks_root}/folder/{folderid}/message/{messageid}?note=

The note can be anything you want it to be.

Change unread flag
------------------

All messages have an unread flag. It signals if the message have been read. e-Boks does not
change this flag automatically. You have to set it manually once you think that the user has
seen the message. To set the unread flag make a PUT request to

::

  {eboks_root}/folder/{folderid}/message/{messageid}?read={read}

where the **read** parameter should be 'true' for marking the message as read and 'false' for
marking it unread. The request tells whether the message should be marked as read, but the value
is negated and stored in the unread flag.

Note that when getting the metadata XML in :ref:`Message metadata`, the unread flag is bugged and
will always be false. It is however not bugged when you search for messages in the
:ref:`Getting Messages` section.

For example, to set a message with the id 20151234, in a folder with id 12345678, as unread, we
send a PUT request to

::

    {eboks_root}/folder/12345678/message/20151234?read=false

Upload message
--------------

E-Boks allows you to upload/send a message to yourself. The message will appear as any other
message in your e-Boks, just like those sent from companies and government bodies.

It seems like the maximum message size is 10MB. Currently e-Boks gives you 1 GB of storage
space for all uploaded messages. It does not modify the message content at all, so you can
upload any binary file and get an exact copy back when you download it.

To upload a message, send a PUT request to

::

  {eboks_root}/folder/{folderid}?name={title}&filetype={filetype}

Where the request body should be the message content and set

**folderid**
    to the id of the folder that you want the message to be placed in.

**title**
    to the title of the message.

**filetype**
    to a 1-4 letter word describing the message type (pdf, html, txt, etc.).

For example, to upload a message saying 'Hello World!', titled 'whatsup', with filetype 'txt', to
a folder with the id 12345678. We send a PUT request with the request body 'Hello World!' to

::

    {eboks_root}/folder/12345678?name=whatsup&filetype=txt
