.. _Senders:

Senders
=======

In this section we see how to get a list of private companies and government bodies 
that have sent you mail in e-Boks. It will show entities that have sent you at least 
one mail. You need to have completed the :ref:`Authentication` section before using 
this.

To get a list of all entities that have sent you mail, make a GET request to

::

    {eboks_root}/folders/search/senders

This will return a response with an XML body of the form

.. code:: xml

    <SearchFolders>
    <SearchFolder name="Danske Bank" unread="0">
        <Sender id="243">
            Danske Bank
        </Sender>
    </SearchFolder>
    <SearchFolder name="Feriepengeinfo" unread="1">
        <Sender id="64">
            Feriepengeinfo - FerieKonto
        </Sender>
    </SearchFolder>
    <SearchFolder name="Horsens Kommune" unread="0">
        <Sender id="185">
            Horsens Kommune
        </Sender>
    </SearchFolder>
    </SearchFolders>

Where you can extract the sender names and id's. The sender id's uniquely identify each sender
and are used in operations involving senders. There is also an **unread** attribute with the
amount of unread messages from each sender.
