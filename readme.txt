# PostDocs

This is unofficial documentation for the rest API used by the official e-Boks 
mobile applications. See https://randominequalities.gitlab.io/PostDocs/ for 
the corresponding website.

The documentation files are reStructuredText (.rst). These can be used to 
generate nice looking documentation in a many different formats. We use the 
Sphinx documentation generator to convert it to a nice looking website.

/docs - the documentation.
/site - the generated website. 
/.gitlab-ci.yml - gitlab script that deploys the website on each commit.

# Required Python Libraries

sphinx
sphinx-rtd-theme

# Generate Website

sphinx-build -n -v -b html docs site